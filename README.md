# copy.js

### [EN-US]
## Easily copy text from elements simply by clicking on them

### Instalation
* Refer the `js/copy.js` file at the end of your `body` tag
      <script src="js/copy.js"></script>

### Enable copy
* Add the dataset `data-copy="something"` in the element that you want to copy
* The value `something` will be copied to the clipboard automatically
* All elements with the dataset will receive the `onClick` event

### Example
    <div data-copy="something"></div>
  - `something` will be copied

### Known Issues
  - Image click, icon position





### [PT-BR]
## Copie textos de elementos facilmente apenas clicando neles

### Instalação
* Referencie o arquivo `js/copy.js` no final de sua tag `body`
      <script src="js/copy.js"></script>

### Habilitar copia
* Adicione o dataset `data-copy="alguma-coisa"` no elemento que deseja copiar
* O valor `alguma-coisa` será copiado para área de transferência automaticamente
* Todos elementos com o dataset receberão o evento `onClick`

### Exemplo
    <div data-copy="alguma-coisa"></div>
  - `alguma-coisa` será copiado

### Erros conhecidos
  - Click em imagem, posição do icone
