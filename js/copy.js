/*
  Author: Akauan GonÃ§alves Resende

  Add this script after the HTML
  Simple clipboard copy button
*/

var copyDataset = 'copy';
var copySelector = '[data-'+copyDataset+']';
var copyElements = document.querySelectorAll(copySelector);

for(var id = 0;copyElements.length > id; id++){
  var element = copyElements[id];
  element.addEventListener('click',copy);
}

function copy() {
  var aux = document.createElement("input");
  aux.setAttribute("value", this.dataset[copyDataset]);
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  document.body.removeChild(aux);

  var htmlEmoji = '&#128203;';
  var box = document.createElement("div");
  box.innerHTML = htmlEmoji;
  box.style.backgroundColor = "#adadad";
  box.style.position = "absolute";
  box.style.zIndex = "99999";
  box.style.border = "2px dotted #6b6b6b";
  box.style.borderRadius = "4px";
  //box.style.height = "25px";
  //box.style.width = "25px";
  //var svgpath = "M 25 0 h -25 v 25 m 30 -20 l -25 0 l 0 25 l 25 0 l 0 -25 m -20 5 h 15 m 0 5 h -15 m 0 5 h 15 m 0 5 h -15 ";
  //box.style.backgroundImage = "url(\"data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(255, 255, 255, 0.5)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='"+svgpath+"'/%3E%3C/svg%3E\")";
  document.body.appendChild(box);

  var elRect = this.getBoundingClientRect();
  var boxRect = box.getBoundingClientRect();

  var pageScroll = [0,0];
  pageScroll[0] = window.pageXOffset;
  pageScroll[1] = window.pageYOffset;

  var boxPosition = [0,0];
  boxPosition[0] = (elRect.left + (elRect.width  * 0.9) - (boxRect.width  * 0.5) + pageScroll[0]);
  boxPosition[1] = (elRect.top  - (elRect.height * 0.8) + (boxRect.height * 0.5) + pageScroll[1]);
  box.style.left = boxPosition[0]+"px";
  box.style.top = boxPosition[1]+"px";

  setTimeout(function(){ document.body.removeChild(box); },1000);
}
